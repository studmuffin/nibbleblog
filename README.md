[Nibbleblog](http://www.nibbleblog.com/)
================================================

Nibbleblog is a powerful engine for creating blogs, all you need is PHP to work.

- [Site](http://www.nibbleblog.com/)
- [Documentation](http://docs.nibbleblog.com/)
- [Help and Support](http://forum.nibbleblog.com/)

Requirements
------------

* PHP v5.2 or higher
* PHP module - DOM
* PHP module - SimpleXML
* PHP module - GD
* Directory “content” writable by Apache/PHP

Optionals requirements

* PHP module - Mcrypt

Installation guide
------------------

1. Download the last version from http://nibbleblog.com
2. Unzip the downloaded file
3. Upload all files to your hosting or local server via FTP, Shell, Cpanel, others.
4. With your browser, go to the URL of your web. Example: www.domain-name.com
5. Complete the form
 
tommygunnz notes "to do by others":

* add ability to restrict access of new users to specific blog features
* additional language support needed for all languages except english for all new/fixed functionality
* update.php and install.php may need slight mods
* fix/replace calls to defunct defensio to stop php from generating error_log warnings
* bump version number

Link: https://github.com/dignajar/nibbleblog/issues/96

Social
------
* Twitter: http://twitter.com/nibbleblog
* Facebook: http://www.facebook.com/nibbleblog
* Google+: http://google.com/+nibbleblog

License
-------
Nibbleblog is opensource software licensed under the [GPL v3](http://www.gnu.org/licenses/gpl-3.0.txt)
